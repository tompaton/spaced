# Spaced

This is a 386 DOS game written in 1996.

The executable is available at http://tompaton.com/archive/old-code/spaced.zip
and can be run using dosbox.

This source code is most C++ with some 386 assembler optimizations and hardware
specific routines.

It was built using Borland C++ 3.1 and TASM.

The graphics are stored in .VGA files, I think makedat.cpp bundles these into
spaced.dat.
