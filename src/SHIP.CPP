#include <stdio.h>

#include "sprite.h"
#include "bullet.h"
#include "ship.h"

int
Ship::isHit( Bullet *bullet)
{
	int px, py;

	if(state == 0)		// inactive
		return 0;

	px = bullet->x;
	py = bullet->y;
	if(px>=x && py>=y && px<=(x+bitmap->sx) && py<=(y+bitmap->sy) )
		return 1;

	px = bullet->x + bullet->bitmap->sx;
	py = bullet->y;
	if(px>=x && py>=y && px<=(x+bitmap->sx) && py<=(y+bitmap->sy) )
		return 1;

	px = bullet->x + bullet->bitmap->sx;
	py = bullet->y + bullet->bitmap->sy;
	if(px>=x && py>=y && px<=(x+bitmap->sx) && py<=(y+bitmap->sy) )
		return 1;

	px = bullet->x;
	py = bullet->y + bullet->bitmap->sy;
	if(px>=x && py>=y && px<=(x+bitmap->sx) && py<=(y+bitmap->sy) )
		return 1;

	return 0;
}


void
Ship::draw(byte *screen)
{
	switch(state)
	{
	  case 0:
					break;
	  case 1:  bitmap->draw(x,y,screen);
					break;
	  case 2:  explosion->draw(x,y,screen);
					break;
	}
}
